<?php


//Route::prefix('/')->group(function () {});

Route::get('/', 'FrontController@Login')->name('admin.login')->middleware('LoginCheck');
//Route::get('/dashboard', 'FrontController@Dashboard')->name('admin.dashboard')->middleware('AdminCheck');
Route::get('/profile', 'FrontController@Profile')->name('admin.profile')->middleware('AdminCheck');

Route::middleware('AdminCheck')->prefix('/orders')->group(function () {
    Route::get('/', 'FrontController@Orders')->name('admin.orders');
    Route::get('/edit/{id}', 'FrontController@OrdersEdit')->name('admin.orders.edit');
    Route::get('/view/{id}', 'FrontController@ProductsView')->name('admin.products.view');
});

Route::middleware('AdministratorUserCheck')->prefix('/users')->group(function () {
    Route::get('/', 'FrontController@Users')->name('admin.users')->middleware('AdminCheck');
    Route::get('/create', 'FrontController@UsersCreate')->name('admin.users.create')->middleware('AdminCheck');
    Route::get('/edit/{id}', 'FrontController@UsersEdit')->name('admin.users.edit')->middleware('AdminCheck');
});

Route::middleware('AdminCheck')->prefix('/products')->group(function () {
    Route::get('/', 'FrontController@Products')->name('admin.products');
    Route::get('/create', 'FrontController@ProductsCreate')->name('admin.products.create')->middleware('AdministratorUserCheck');
    Route::get('/edit/{id}', 'FrontController@ProductsEdit')->name('admin.products.edit')->middleware('AdministratorUserCheck');
    Route::get('/view/{id}', 'FrontController@ProductsView')->name('admin.products.view');
});


Route::prefix('app/v0.01')->group(function () {
    Route::post('/login', 'AuthController@login')->name('api.user.login');
    Route::get('/logout', 'AuthController@logout')->name('api.user.logout');
    Route::post('/reset-password', 'AuthController@ResetPassword')->name('api.user.reset.password');

    Route::get('/clients', 'AdminController@ClientsGetAll')->name('api.clients.get.all');
    Route::get('/profiles', 'AdminController@ProfilesGetAll')->name('api.profiles.get.all');
    Route::get('/locations', 'AdminController@LocationsGetAll')->name('api.locations.get.all');

    Route::prefix('/admin')->group(function () {
        Route::get('/', 'AdminController@UserGetInfo')->name('api.admin.get.info');
        Route::put('/', 'AdminController@UserUpdateInfo')->name('api.admin.update.info');
    });

    Route::prefix('/user')->group(function () {
        Route::post('/', 'AdminController@UserCreate')->name('api.user.create');
        Route::put('/', 'AdminController@UserEdit')->name('api.user.edit');
        Route::get('/', 'AdminController@UserGetAll')->name('api.user.get.all');
        Route::get('/single', 'AdminController@UserGetSingle')->name('api.user.get.single');
        Route::delete('/', 'AdminController@UserDelete')->name('api.user.delete');
    });

    Route::prefix('/products')->group(function () {
        Route::post('/', 'AdminController@ProductCreate')->name('api.products.create');
        Route::put('/', 'AdminController@ProductEdit')->name('api.products.edit');
        Route::get('/', 'AdminController@ProductGetAll')->name('api.products.get.all');
        Route::get('/single', 'AdminController@ProductGetSingle')->name('api.products.get.single');
        Route::delete('/', 'AdminController@ProductDelete')->name('api.products.delete');
    });

    Route::prefix('/order')->group(function () {
        Route::post('/', 'AdminController@OrderCreate')->name('api.order.create');
        Route::put('/', 'AdminController@OrderEdit')->name('api.order.edit');
        Route::get('/', 'AdminController@OrderGetAll')->name('api.order.get.all');
        Route::get('/single', 'AdminController@OrderGetSingle')->name('api.order.get.single');
    });
});