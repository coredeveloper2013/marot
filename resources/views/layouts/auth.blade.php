<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8" />
    <title>
        {{$page}}
    </title>

    <meta name="title" content="Marot">
    <meta name="og:title" content="Marot">
    <meta name="image" content="{{env('ASSETS_PATH')}}/image/logo_m.png">
    <meta name="og:image" content="{{env('ASSETS_PATH')}}/image/logo_m.png">

    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{env('ASSETS_PATH')}}/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{env('ASSETS_PATH')}}/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

    <script src="{{env('ASSETS_PATH')}}/plugins/jQuery/jquery-3.3.1.min.js" type="application/javascript"></script>
    <script src="{{env('ASSETS_PATH')}}/plugins/vue/vue.min.js" type="application/javascript"></script>


    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{env('ASSETS_PATH')}}/image/favicon.png" />
    <link href="{{env('ASSETS_PATH')}}/stylesheets/marot.min.css" rel="stylesheet" type="text/css" />
    @yield('resources')
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default marot">
<!-- begin:: Page -->
@yield('content')
<!-- end:: Page -->
<!--begin::Base Scripts -->
<script src="{{env('ASSETS_PATH')}}/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="{{env('ASSETS_PATH')}}/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<!--end::Base Scripts -->
<!--begin::Page Snippets -->
{{--<script src="{{env('ASSETS_PATH')}}/snippets/pages/user/login.js" type="text/javascript"></script>--}}
<!--end::Page Snippets -->
</body>
<!-- end::Body -->
</html>
