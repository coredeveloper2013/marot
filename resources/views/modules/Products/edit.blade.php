@extends('layouts.structure')

@section('additionalCSS')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
@endsection

@section('resources')
<link rel="stylesheet" href="{{ env('ASSETS_PATH') }}/alertify/css/alertify.min.css" type="text/css" />
@endsection

@section('content')
    <div class="m-content" id="Vue_component_main">
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Create New Product
                        </h3>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            <form @submit.prevent="ProductsUpdate">
                <div class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label>Product name</label>
                                    <div class="has-danger">
                                        <input name="name" v-model="EditForm.name" type="text" class="form-control m-input m-input--square" placeholder="Product name">
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Code Product</label>
                                    <div class="has-danger">
                                        <input name="code_product" v-model="EditForm.code_product" type="text" class="form-control m-input m-input--square" placeholder="Product name">
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Product Description</label>
                                    <div class="has-danger">
                                        <textarea name="description" id="description"v-model="EditForm.description" class="form-control m-input m-input--square summerNote" placeholder="Product Description"></textarea>
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">&nbsp&nbsp&nbsp&nbsp -</div>
                                <div class="form-group m-form__group">
                                    <label>Information Order</label>
                                    <div class="has-danger">
                                        <textarea name="information_order" v-model="EditForm.information_order" class="form-control m-input m-input--square summerNote" placeholder="Information Order"></textarea>
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>

                                {{-- <div class="form-group m-form__group">
                                    <label>Minimum number of order</label>
                                    <div class="has-danger">
                                        <input name="minimum_orders" v-model="EditForm.minimum_orders" type="text" class="form-control m-input m-input--square" placeholder="Minimum number of order">
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div> --}}

                                <div class="form-group m-form__group">
                                    <div class="row" v-for="(quantityPrice, index) in EditForm.quantity_price">
                                        <div class="col-4">
                                            <label>Quantity</label>
                                            <div class="has-danger">
                                                <input v-model="quantityPrice.quantity" type="text" class="form-control m-input m-input--square" value="">
                                                <div class="form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <label>Price</label>
                                            <div class="has-danger">
                                                <input v-model="quantityPrice.price" type="text" class="form-control m-input m-input--square" value="">
                                                <div class="form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <button class="btn btn-danger btn-row-remove" @click.prevent="removeRow(quantityPrice, index)"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <label>Quantity</label>
                                            <div class="has-danger">
                                                <input v-model="EditForm.slash_quantity" type="text" class="form-control m-input m-input--square" value="/" readonly>
                                                <div class="form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <label>Price</label>
                                            <div class="has-danger">
                                                <input v-model="EditForm.slash_price" type="text" class="form-control m-input m-input--square" value="0">
                                                <div class="form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-primary" @click.prevent="addMoreQty()">Add more quantity and price</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">&nbsp&nbsp&nbsp&nbsp -</div>
                                <div class="form-group m-form__group">
                                    <label>Client</label>
                                    <div class="has-danger">
                                        <select v-model="EditForm.client_id" name="client_id" class="form-control m-input">
                                            <option value="">Select Client</option>
                                            <option v-for="client in clients" :value="client.id" v-text="client.name"></option>
                                        </select>
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Product Tags</label>
                                    <div class="has-danger">
                                        <textarea name="tags" v-model="EditForm.tags" type="text" class="form-control m-input m-input--square" rows="15" placeholder="tag1, tag2, tag3"></textarea>
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="margin-5"></div>
                                <div class="form-group m-form__group">
                                    <div v-if="EditForm.file_path.length === 0" class="square-box">
                                        <div class="square-content">
                                            <div v-if="UploadProcess === false" class="inner-wrapper has-danger">
                                                <a href="#" class="btn btn-outline-danger m-btn m-btn--custom m-btn--pill m-btn--air outline-btn">
                                                    <span>
                                                        <span>Add image</span>
                                                    </span>
                                                    <input name="file_path" type="file" @change="ImageUpload" class="image-picker">
                                                </a>
                                                <div class="form-control-feedback"></div>
                                            </div>
                                            <div v-else class="inner-wrapper">
                                                <div class="m-spinner m-spinner--danger m-spinner--lg"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-if="EditForm.file_path.length !== 0" class="square-box preview">
                                        <div class="square-content" :style="{ backgroundImage: 'url({{env('STORAGE_URL')}}image/' + EditForm.file_path + ')' }">
                                            <div v-if="UploadProcess === false" class="inner-wrapper">
                                                <a href="#" class="btn btn-danger m-btn m-btn--custom m-btn--pill m-btn--air ma_btn">
                                                    <span>
                                                        <span>Change image</span>
                                                    </span>
                                                    <input type="file" @change="ImageUpload" class="image-picker">
                                                </a>
                                            </div>
                                            <div v-else class="inner-wrapper">
                                                <div class="m-spinner m-spinner--danger m-spinner--lg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="margin-20"></div>
                        <div class="margin-20"></div>
                        <div class="form-group m-form__group text-right">
                            <a href="{{route('admin.products')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air ma_btn gray">
                            <span>
                                <i class="fa fa-times-circle"></i>
                                <span>Cancel</span>
                            </span>
                            </a>
                            <button type="submit" class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air ma_btn"
                                    :disabled="HttpRequest === true"
                                    :class="{'m-loader m-loader--right m-loader--light': HttpRequest === true}">
                            <span>
                                <i class="fa fa-check-circle"></i>
                                <span>Update</span>
                            </span>
                            </button>
                        </div>
                        <hr>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
    <script src="{{ env('ASSETS_PATH') }}/alertify/alertify.min.js"></script>
    <script>
        new Vue({
            el: '#Vue_component_main',
            data: {
                Api_path: '{{env('API_PATH')}}',
                EditForm: {
                    name: '',
                    code_product: '',
                    description: '',
                    information_order: '',
                    minimum_orders: 0,
                    quantity_price: [
                        { quantity: 0, price: 0 }
                    ],
                    slash_quantity: '/',
                    slash_price: 0,
                    client_id: '',
                    file_path: '',
                    tags: ''
                },
                quantityPriceData: [],
                UploadProcess: false,
                HttpRequest: false,
                clients: [],
                product_id: '{{$product_id}}',
            },
            methods: {
                GetUser: function () {
                    let _this = this;
                    $.ajax({
                        url: _this.Api_path+"/products/single",
                        type: "get",
                        data: {id: _this.product_id},
                        success: function (response) {
                            if (response.status === 2000) {
                                _this.EditForm.id = response.data.id;
                                _this.EditForm.name = response.data.name;
                                _this.EditForm.code_product = response.data.code_product;
                                _this.EditForm.description = response.data.description;
                                _this.EditForm.information_order = response.data.information_order;
                                _this.EditForm.minimum_orders = response.data.minimum_orders;
                                _this.EditForm.client_id = response.data.client_id;
                                _this.EditForm.file_path = response.data.file_path;
                                _this.EditForm.tags = response.data.tags;
                                _this.quantityPriceData = response.quantityPricesData;
                                // Initialize quantity and price after ajax calling data getting
                                let qtyPriceData = _this.quantityPriceData;
                                _this.EditForm.quantity_price = [];
                                for ( let itemData in qtyPriceData ) {
                                    if ( qtyPriceData[itemData].quantity == 900000 ) { // This is slash line
                                        _this.EditForm.slash_quantity = '/';
                                        _this.EditForm.slash_price = qtyPriceData[itemData].price;
                                    }
                                    else {
                                        _this.EditForm.quantity_price.push( { 'quantity': qtyPriceData[itemData].quantity, 'price': qtyPriceData[itemData].price } );
                                    }
                                }
                            }
                            setTimeout(function(){ 
                                $('.summerNote').summernote({
                                    height:300,
                                    callbacks: {
                                        onKeydown: function(e) {   
                                            _this.EditForm.description = $('textarea[name="description"]').val();
                                            _this.EditForm.information_order = $('textarea[name="information_order"]').val();
                                        },
                                        onBlur: function(e) {      
                                            _this.EditForm.description = $('textarea[name="description"]').val();
                                            _this.EditForm.information_order = $('textarea[name="information_order"]').val();
                                        },
                                        onChange: function(e) {  
                                            _this.EditForm.description = $('textarea[name="description"]').val();
                                            _this.EditForm.information_order = $('textarea[name="information_order"]').val();
                                        }
                                    }
                                });
                            }, 500);
                        }
                    });
                },
                GetClients: function () {
                    let _this = this;
                    $.ajax({
                        url: _this.Api_path+"/clients",
                        type: "get",
                        success: function (response) {
                            if (response.status === 2000) {
                                _this.clients = response.data;
                            }
                        }
                    });
                },
                ImageUpload: function (event) {
                    const _this = this;
                    _this.UploadProcess = true;
                    let trigger = $(event.target);
                    let input = event.target.files[0];
                    let formData = new FormData();
                    formData.append("module_type", 1);
                    formData.append("media_type", 1);
                    formData.append("image", input);
                    $.ajax({
                        type: "POST",
                        url: _this.Api_path + '/media',
                        data: formData,
                        processData: false,
                        contentType: false,
                        xhr: function () {
                            let xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    let percentComplete = (evt.loaded / evt.total) * 100;
                                    // console.log(percentComplete);
                                }
                            }, false);
                            return xhr;
                        },
                        success: function (res) {
                            if (parseInt(res.status) === 2000) {
                                _this.UploadProcess = false;
                                _this.EditForm.file_path = res.data.file_path;
                            }
                        }
                    });
                },
                ProductsUpdate: function () {
                    let _this = this;
                    _this.HttpRequest = true;
                    $('.form-control-feedback').html('');
                    if ( _this.EditForm.quantity_price.length > 0 ) {
                        $.ajax({
                            url: _this.Api_path+"/products",
                            type: "put",
                            data: _this.EditForm,
                            success: function (response) {
                                _this.HttpRequest = false;
                                if (response.status === 2000) {
                                    window.location.href = '{{route('admin.products')}}';
                                }else{
                                    alertify.set('notifier', 'position', 'top-right');
                                    alertify.error(response.error);
                                    _this.ErrorHandaler(response.error);
                                }
                            }
                        });
                    }
                    else {
                        alertify.set('notifier', 'position', 'top-right');
                        alertify.error('Quantity and price cannot be empty!');
                    }
                },
                ErrorHandaler: function (errors) {
                    $.each(errors, function (i, v) {
                        if(i === 'file_path'){
                            v = 'Product image is required'
                        }
                        $('[name='+i+']').closest('.has-danger').find('.form-control-feedback').html(v);
                    });
                },
                addMoreQty: function() {
                    let _this = this;
                    _this.EditForm.quantity_price.push( { 'quantity': 0, 'price': 0 } );
                },
                removeRow: function(row, index) {
                    let _this = this;
                    _this.EditForm.quantity_price.splice(index, 1);
                },
            },
            created(){
                this.GetClients();
                this.GetUser();
            },
            mounted(){
                let THIS = this;
            }
        });
    </script>
@endsection