@extends('layouts.structure')

@section('resources')

@endsection

@section('content')
    <div class="m-content" id="Vue_component_main">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    {{$page}}
                                </h3>
                            </div>
                        </div>
                        <div class="col-md-7 text-right">
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <input @keyup="Search" v-model="GetConfig.keyword" type="text"
                                           class="form-control m-input m-input--square" placeholder="Search here...">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Section-->
                <div class="m-section">
                    <div class="m-section__content">
                        <table class="table table-striped m-table head-sm">
                            <thead>
                            <tr>
                                <th>
                                    <div @click="filterData('id')" class="pointer">
                                        <span>ID</span>&nbsp;
                                        <span><i class="fa fa-sort"></i></span>
                                    </div>
                                </th>
                                <th style="min-width: 250px">
                                    <div @click="filterData('name')" class="pointer">
                                        <span>Product</span>&nbsp;
                                        <span><i class="fa fa-sort"></i></span>
                                    </div>
                                </th>
                                <th>
                                    <div @click="filterData('order_code')" class="pointer">
                                        <span>Order Code</span>&nbsp;
                                        <span><i class="fa fa-sort"></i></span>
                                    </div>
                                </th>
                                <th>
                                    <div @click="filterData('code_product')" class="pointer">
                                        <span>Product Code</span>&nbsp;
                                        <span><i class="fa fa-sort"></i></span>
                                    </div>
                                </th>
                                <th>
                                    <div @click="filterData('units')" class="pointer">
                                        <span>Units</span>&nbsp;
                                        <span><i class="fa fa-sort"></i></span>
                                    </div>
                                </th>
                                <th>
                                    <div @click="filterData('user_name')" class="pointer">
                                        <span>User</span>&nbsp;
                                        <span><i class="fa fa-sort"></i></span>
                                    </div>
                                </th>
                                <th>
                                    @if($Auth['user_type'] == 1)
                                        <div @click="filterData('client')" class="pointer">
                                            <span>Client</span>&nbsp;
                                            <span><i class="fa fa-sort"></i></span>
                                        </div>
                                    @else
                                        {{-- <div @click="filterData('order_code')" class="pointer">
                                            <span>Order Code</span>&nbsp;
                                            <span><i class="fa fa-sort"></i></span>
                                        </div> --}}
                                    @endif
                                </th>
                                <th>
                                    <div @click="filterData('date')" class="pointer">
                                        <span>Date</span>&nbsp;
                                        <span><i class="fa fa-sort"></i></span>
                                    </div>
                                </th>
                                <th>
                                    Ticket Status
                                </th>
                                <th>
                                    Edit
                                </th>
                                @if($Auth['user_type'] == 1)
                                    <th>
                                        Visibility
                                    </th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                                <tr v-if="Orders.length !== 0" v-for="(order, index) in Orders">
                                <td v-text="order.id"></td>
                                <td v-text="order.product_name" style="min-width: 250px"></td>
                                <td v-text="order.order_code"></td>
                                <td v-text="order.code_product"></td>
                                <td v-text="order.units"></td>
                                <td v-text="order.user_name"></td>
                                @if($Auth['user_type'] == 1)
                                    <td v-text="order.client_name"></td>
                                @else
                                    <td></td>
                                    {{-- <td>
                                        <span v-if="order.order_code.length == 0"> -- </span>
                                        <span v-else v-text="order.order_code"></span>
                                    </td> --}}
                                @endif
                                <td v-text="order.formatted_date"></td>
                                <td>
                                    <button v-if="order.status == 1"
                                            class="btn btn-metal m-btn btn-sm m-btn--pill status-btn in_process">
                                        In Progress
                                    </button>
                                    <button v-if="order.status == 2"
                                            class="btn btn-metal m-btn btn-sm m-btn--pill status-btn in_print">
                                        In Print
                                    </button>
                                    <button v-if="order.status == 3"
                                            class="btn btn-metal m-btn btn-sm m-btn--pill status-btn delivered">
                                        Delivered
                                    </button>
                                </td>
                                <td>
                                    <a :href="'{{env('ROUTE_URL')}}/orders/edit/'+order.id"
                                       class="btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill ma_btn info">
                                        <i class="la la-pencil"></i>
                                    </a>
                                </td>
                                @if($Auth['user_type'] == 1)
                                    <td class="text-center">
                                        <button @click="UpdateTicket($event, order.id, index)"
                                                class="btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill ma_btn info">
                                            <i v-if="order.visibility == 1" class="la la-eye"></i>
                                            <i v-if="order.visibility == 2" class="la la-eye-slash"></i>
                                        </button>
                                    </td>
                                @endif
                            </tr>
                                <tr v-if="Orders.length == 0">
                                    <td colspan="{{$Auth['user_type'] == 1 ? 10 : 9}}">
                                        <div class="m-alert m-alert--outline alert alert-success alert-dismissible fade show text-center" role="alert">
                                            Your Order list is empty.
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="pagination" v-if="GetConfig.pageCount > 1">
                        <button @click="navPagination(1)"
                                class="btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only"
                                :disabled="GetConfig.pageNo == 1">
                            <i class="la la-angle-left"></i>
                        </button>
                        <button v-for="count in GetConfig.pageCount"
                                class="btn btn-metal btn-sm m-btn  m-btn m-btn--icon" style="margin: 0 3px;"
                                :class="{'ma_btn': count == GetConfig.pageNo}" @click="goToPage(count)">
                            <span v-text="count"></span>
                        </button>
                        <button @click="navPagination(2)"
                                class="btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only"
                                :disabled="GetConfig.pageNo == GetConfig.pageCount">
                            <i class="la la-angle-right"></i>
                        </button>
                    </div>
                </div>
                <!--end::Section-->
            </div>
            <!--end::Form-->
        </div>
    </div>
    
    <script>
        new Vue({
            el: '#Vue_component_main',
            data: {
                Api_path: '{{env('API_PATH')}}',
                Orders: [],
                GetConfig: {
                    pageNo: 1,
                    limit: 15,
                    keyword: '',
                    filter: '',
                    filter_type: '',
                    pageCount: 0,
                }
            },
            methods: {
                GetOrders: function () {
                    let _this = this;
                    _this.GetConfig.pageCount = 0;
                    $.ajax({
                        url: _this.Api_path + "/order",
                        type: "get",
                        data: _this.GetConfig,
                        success: function (response) {
                            _this.Orders = response.data;
                            _this.GetConfig.pageCount = Math.ceil(response.totalData / _this.GetConfig.limit);
                        }
                    });
                },
                goToPage: function (pageNo) {
                    this.GetConfig.pageNo = pageNo;
                    this.GetOrders();
                },
                navPagination: function (type) {
                    if (type === 1) {
                        if (this.GetConfig.pageNo !== 1) {
                            this.GetConfig.pageNo--;
                        }
                    } else {
                        if (this.GetConfig.pageNo !== this.GetConfig.pageCount) {
                            this.GetConfig.pageNo++;
                        }
                    }
                    this.GetOrders();
                },
                UpdateTicket: function (event, id, index) {
                    let _this = this;
                    let trigger = $(event.target);
                    if (trigger.hasClass('la')) {
                        trigger = trigger.closest('.btn')
                    }
                    let FormData = {
                        id: id,
                        visibility: 0
                    };
                    if (_this.Orders[index].visibility === 1) {
                        this.Orders[index].visibility = 2;
                        FormData.visibility = 2;
                    } else {
                        this.Orders[index].visibility = 1;
                        FormData.visibility = 1;
                    }
                    trigger.attr('disabled', 'disabled');
                    $.ajax({
                        url: _this.Api_path + "/order",
                        type: "put",
                        data: FormData,
                        success: function (response) {
                            trigger.removeAttr("disabled");
                        }
                    });
                },
                Search: function (pageNo) {
                    this.GetConfig.pageNo = 1;
                    this.GetOrders();
                },
                filterData: function (data) {
                    this.GetConfig.filter = data;
                    if (this.GetConfig.filter_type.length === 0) {
                        this.GetConfig.filter_type = 'DESC';
                    } else if (this.GetConfig.filter_type === 'DESC') {
                        this.GetConfig.filter_type = 'ASC';
                    } else if (this.GetConfig.filter_type === 'ASC') {
                        this.GetConfig.filter_type = 'DESC';
                    }
                    this.GetOrders();
                },
                DeleteItem: function (user_id) {
                    let _this = this;
                    swal({
                        title: "Are you sure?",
                        text: "Once Removed, you will not be able to recover this product!",
                        icon: "warning",
                        buttons: ['Cancel', 'Remove'],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: _this.Api_path + "/products",
                                type: "delete",
                                data: {id: user_id},
                                success: function (response) {
                                    if (response.status === 2000) {
                                        _this.GetProducts();
                                        swal("Product has been Removed successfully!", {
                                            icon: "success",
                                        });
                                    }
                                }
                            });
                        } else {
                            swal("Product is safe!", {
                                icon: "info",
                            });
                        }
                    });
                }
            },
            created() {
                this.GetOrders();
            }
        });
    </script>
@endsection