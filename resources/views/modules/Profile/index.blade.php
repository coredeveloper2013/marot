@extends('layouts.structure')

@section('resources')

@endsection

@section('content')
    <div class="m-content" id="Vue_component_main">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
											<span class="m-portlet__head-icon m--hide">
												<i class="la la-gear"></i>
											</span>
                                <h3 class="m-portlet__head-text">
                                    Personal Information
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body" :class="{'disable-section': HttpRequest === true}">
                            <form @submit.prevent="UpdateUser">
                                <div class="form-group m-form__group">
                                    <div class="avatar-picker-wrapper">
                                        <div v-if="CreateForm.avatar.length === 0" class="avatar-picker">
                                            <div v-if="UploadProcess === false" class="inner-wrapper">
                                                <a href="#" class="link-active">Add Avatar</a>
                                            </div>
                                            <div v-else class="inner-wrapper">
                                                <div class="m-spinner m-spinner--danger m-spinner--lg"></div>
                                            </div>
                                            <input type="file" class="image-picker" @change="ImageUpload">
                                        </div>
                                        <div v-else class="avatar-picker"
                                             :style="{ backgroundImage: 'url({{env('STORAGE_URL')}}image/' + CreateForm.avatar + ')' }">
                                            <div v-if="UploadProcess === false" class="inner-wrapper">
                                                <div class="margin-20"></div>
                                                <div class="margin-20"></div>
                                                <div class="margin-20"></div>
                                                <div class="btn m-btn--pill btn-primary btn-sm ma_btn">
                                                    Change Avatar
                                                </div>
                                            </div>
                                            <div v-else class="inner-wrapper">
                                                <div class="m-spinner m-spinner--danger m-spinner--lg"></div>
                                            </div>
                                            <input type="file" class="image-picker" @change="ImageUpload">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label text-left">
                                        Name
                                    </label>
                                    <div class="col-9 has-danger">
                                        <label v-text="CreateForm.name" class="col-form-label"></label>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label text-left">
                                        Email
                                    </label>
                                    <div class="col-9 has-danger">
                                        <label v-text="CreateForm.email" class="col-form-label"></label>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label text-left">
                                        Telephone
                                    </label>
                                    <div class="col-9 has-danger">
                                        <label v-text="CreateForm.phone" class="col-form-label"></label>
                                    </div>
                                </div>
                                <div v-if="CreateForm.location != null" class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label text-left">
                                        Location/Office
                                    </label>
                                    <div class="col-9 has-danger">
                                        <select v-model="CreateForm.location" name="location"
                                                class="form-control m-input">
                                            <option value="">Select location</option>
                                            <option v-for="location in locations" :value="location.id"
                                                    v-text="location.name"></option>
                                        </select>
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <hr>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label text-left">
                                        Password
                                    </label>
                                    <div class="col-9 has-danger">
                                        <input v-model="CreateForm.password" name="password" type="password"
                                               class="form-control m-input m-input--square" placeholder="Password">
                                        <div class="form-control-feedback"></div>
                                        <span class="m-form__help">Enter password if you want to update password</span>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label text-left">
                                        Confirm Password
                                    </label>
                                    <div class="col-9 has-danger">
                                        <input v-model="CreateForm.password_confirmation" type="password"
                                               class="form-control m-input m-input--square" placeholder="Password">
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group m-form__group text-right">
                                    <a href="{{route('admin.orders')}}"
                                       class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air ma_btn gray">
                                        <span>
                                            <i class="fa fa-times-circle"></i>
                                            <span>Cancel</span>
                                        </span>
                                    </a>
                                    <button type="submit"
                                            class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air ma_btn"
                                            :disabled="HttpRequest === true"
                                            :class="{'m-loader m-loader--right m-loader--light': HttpRequest === true}">
                                        <span>
                                            <i class="fa fa-check-circle"></i>
                                            <span>Save</span>
                                        </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
    <script>
        new Vue({
            el: '#Vue_component_main',
            data: {
                Api_path: '{{env('API_PATH')}}',
                CreateForm: {
                    id: '',
                    avatar: '',
                    name: '',
                    email: '',
                    phone: '',
                    location: null,
                    password: '',
                    password_confirmation: ''
                },
                HttpRequest: false,
                UploadProcess: false,
                locations: [],
            },
            methods: {
                GetLocations: function () {
                    let _this = this;
                    $.ajax({
                        url: _this.Api_path + "/locations",
                        type: "get",
                        success: function (response) {
                            if (response.status === 2000) {
                                _this.locations = response.data;
                            }
                        }
                    });
                },
                GetUser: function () {
                    let _this = this;
                    $.ajax({
                        url: _this.Api_path + "/admin",
                        type: "get",
                        success: function (response) {
                            if (response.status === 2000) {
                                _this.CreateForm.id = response.data.id;
                                _this.CreateForm.avatar = response.data.avatar == null ? '' : response.data.avatar;
                                _this.CreateForm.name = response.data.name;
                                _this.CreateForm.email = response.data.email;
                                _this.CreateForm.phone = response.data.phone;
                                _this.CreateForm.location = response.data.location;
                                console.log(_this.CreateForm.avatar);
                            }
                        }
                    });
                },
                UpdateUser: function () {
                    let _this = this;
                    _this.HttpRequest = true;
                    $('.form-control-feedback').html('');
                    $.ajax({
                        url: _this.Api_path + "/admin",
                        type: "put",
                        data: _this.CreateForm,
                        success: function (response) {
                            _this.HttpRequest = false;
                            if (response.status === 2000) {
                                window.location.href = '';
                            } else {
                                _this.ErrorHandaler(response.error)
                            }
                        }
                    });
                },
                ErrorHandaler: function (errors) {
                    $.each(errors, function (i, v) {
                        $('[name=' + i + ']').closest('.has-danger').find('.form-control-feedback').html(v);
                    })

                    // console.log(errors);
                },
                ImageUpload: function (event) {
                    const _this = this;
                    _this.UploadProcess = true;
                    let trigger = $(event.target);
                    let input = event.target.files[0];
                    let formData = new FormData();
                    formData.append("module_type", 1);
                    formData.append("media_type", 1);
                    formData.append("image", input);
                    $.ajax({
                        type: "POST",
                        url: _this.Api_path + '/media',
                        data: formData,
                        processData: false,
                        contentType: false,
                        xhr: function () {
                            let xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    let percentComplete = (evt.loaded / evt.total) * 100;
                                    console.log(percentComplete);
                                }
                            }, false);
                            return xhr;
                        },
                        success: function (res) {
                            if (parseInt(res.status) === 2000) {
                                _this.UploadProcess = false;
                                _this.CreateForm.avatar = res.data.file_path;
                            }
                        }

                    });
                },
            },
            created() {
                this.GetUser();
                this.GetLocations();
            }
        });
    </script>
@endsection