@extends('layouts.auth')

@section('resources')

@endsection

@section('content')
    <div class="m-grid m-grid--hor m-grid--root m-page" id="Login_Vue">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--2 m-login-2--skin-2
                    m-login--signin" id="m_login_marot">
            <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
                <div class="m-login__container">
                    <div class="m-login__logo">
                        <a href="#">
                            {{--<img src="{{env('ASSETS_PATH')}}/app/media/img//logos/logo-1.png">--}}
                            <img src="{{env('ASSETS_PATH')}}/image/logo_m.png">
                        </a>
                    </div>
                    <div class="m-login__signin">
                        <div class="m-login__head">
                            <h3 class="m-login__title">
                                Sign In To Marot
                            </h3>
                        </div>
                        <form class="m-login__form m-form" @submit.prevent="Login">
                            <div v-if="requestError.login == true"
                                 class="m-alert m-alert--outline alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                <span>Incorrect email or password. Please try again.</span>
                            </div>
                            <div v-if="resetSuccess == true" class="m-alert m-alert--outline alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                <span>New password sent to your email.</span>
                            </div>
                            <div class="form-group m-form__group">
                                <input v-model="LoginForm.email" class="form-control m-input" type="text" placeholder="Email" autocomplete="off">
                            </div>
                            <div class="form-group m-form__group">
                                <input v-model="LoginForm.password" class="form-control m-input m-login__form-input--last" type="password" placeholder="Password">
                            </div>
                            <div class="row m-login__form-sub">
                                <div class="col m--align-left m-login__form-left">
                                    <label class="m-checkbox  m-checkbox--focus">
                                        <input v-model="LoginForm.remember" type="checkbox" name="remember">
                                        Remember me
                                        <span></span>
                                    </label>
                                </div>
                                <div class="col m--align-right m-login__form-right">
                                    <a href="javascript:;" class="m-link" @click="displayForgetPasswordForm">
                                        Forget Password ?
                                    </a>
                                </div>
                            </div>
                            <div class="m-login__form-action">
                                <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary ma_btn"
                                        :disabled="HttpRequest === true"
                                        :class="{'m-loader m-loader--right m-loader--light': HttpRequest === true}">
                                    Sign In
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="m-login__forget-password">
                        <div class="m-login__head">
                            <h3 class="m-login__title">
                                Forgotten Password ?
                            </h3>
                            <div class="m-login__desc">
                                Enter your email to reset your password:
                            </div>
                        </div>
                        <form class="m-login__form m-form" @submit.prevent="ResetPassword">
                            <div class="form-group m-form__group">
                                <div class="has-danger">
                                    <input v-model="resetForm.email" class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
                                    <div class="form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="m-login__form-action">
                                <button class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary ma_btn"
                                        :disabled="HttpRequestReset === true"
                                        :class="{'m-loader m-loader--right m-loader--light': HttpRequestReset === true}">
                                    Request
                                </button>
                                &nbsp;&nbsp;
                                <button @click="displaySignInForm" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom m-login__btn outline-btn">
                                    Cancel
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        new Vue({
            el: '#Login_Vue',
            data: {
                Api_path: '{{env('API_PATH')}}',
                LoginForm: {
                    email: '',
                    password: '',
                    remember: ''
                },
                HttpRequest: false,
                HttpRequestReset: false,
                requestError: {
                    login: false
                },
                resetForm:{
                    email: ''
                },
                resetSuccess: false,
            },
            methods: {
                Login: function () {
                    let _this = this;
                    _this.HttpRequest = true;
                    _this.resetSuccess = false;
                    _this.requestError.login = false;
                    $.ajax({
                        url: _this.Api_path + "/login",
                        type: "post",
                        data: _this.LoginForm,
                        success: function (response) {
                            _this.HttpRequest = false;
                            if (response.status === 2000) {
                                window.location.href = '{{route('admin.orders')}}';
                            } else {
                                _this.requestError.login = true;
                            }
                        }
                    });
                },
                ResetPassword: function () {
                    let _this = this;
                    _this.HttpRequestReset = true;
                    $('.form-control-feedback').html('');
                    $.ajax({
                        url: _this.Api_path + "/reset-password",
                        type: "post",
                        data: _this.resetForm,
                        success: function (response) {
                            _this.HttpRequestReset = false;
                            if(response.status === 2000){
                                _this.resetSuccess = true;
                                _this.LoginForm.email = _this.resetForm.email;
                                _this.displaySignInForm();
                            }else{
                                _this.ErrorHandaler(response.error)
                            }

                        }
                    });
                },
                ErrorHandaler: function (errors) {
                    $.each(errors, function (i, v) {
                        if(v == 'The email must be a valid email address.'){
                            v = 'Account doesn\'t exist';
                        }
                        $('[name='+i+']').closest('.has-danger').find('.form-control-feedback').html(v);
                    })
                },
                displaySignInForm: function () {
                    const login = $('#m_login_marot');
                    $('.form-control-feedback').html('');
                    login.removeClass('m-login--forget-password');
                    login.removeClass('m-login--signup');
                    login.addClass('m-login--signin');
                    login.find('.m-login__signin').animateClass('flipInX animated');
                },
                displayForgetPasswordForm: function () {
                    const login = $('#m_login_marot');
                    $('.form-control-feedback').html('');
                    login.removeClass('m-login--signin');
                    login.removeClass('m-login--signup');
                    login.addClass('m-login--forget-password');
                    login.find('.m-login__forget-password').animateClass('flipInX animated');
                },
            }
        });
    </script>
@endsection