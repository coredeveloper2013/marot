@extends('layouts.structure')

@section('resources')

@endsection

@section('content')
    <div class="m-content marot" id="Vue_component_main">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    {{$page}}
                                </h3>
                            </div>
                        </div>
                        <div class="col-md-7 text-right">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="{{route('admin.users.create')}}" style="padding: -0.25rem 0.8rem"
                                       class="btn btn-primary m-btn m-btn--custom m-btn--icon btn-sm m-btn--pill m-btn--air ma_btn">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Create New Profile</span>
                                        </span>
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <input @keyup="Search" v-model="GetConfig.keyword" type="text"
                                           class="form-control m-input m-input--square"
                                           placeholder="Search here...">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Section-->
                <div class="m-section">
                    <div class="m-section__content">
                        <table class="table table-striped m-table">
                            <thead>
                            <tr>
                                <th>
                                    <div @click="filterData('email')" class="pointer">
                                        <span>Email</span>
                                        &nbsp;
                                        <span><i class="fa fa-sort"></i></span>
                                    </div>
                                </th>
                                <th>
                                    <div @click="filterData('client')" class="pointer">
                                        <span>Client</span>
                                        &nbsp;
                                        <span><i class="fa fa-sort"></i></span>
                                    </div>
                                </th>
                                <th class="text-right">

                                </th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr v-if="users.length !== 0" v-for="user in users">
                                <td v-text="user.email"></td>
                                <td v-text="user.client_name"></td>
                                <td class="text-right">
                                    <a :href="'{{env('ROUTE_URL')}}/users/edit/'+user.id"
                                       class="btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill ma_btn info">
                                        <i class="la la-pencil"></i>
                                    </a>
                                    <button @click="DeleteItem(user.id)"
                                            class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill">
                                        <i class="la la-trash"></i>
                                    </button>
                                </td>
                            </tr>
                                <tr v-if="users.length == 0">
                                    <td colspan="3">
                                        <div class="m-alert m-alert--outline alert alert-success alert-dismissible fade show text-center" role="alert">
                                            User list is empty.
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="pagination" v-if="GetConfig.pageCount > 1">
                            <button @click="navPagination(1)" class="btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only"
                                    :disabled="GetConfig.pageNo == 1">
                                <i class="la la-angle-left"></i>
                            </button>
                            <button v-for="count in GetConfig.pageCount"
                                    class="btn btn-metal btn-sm m-btn  m-btn m-btn--icon" style="margin: 0 3px;"
                                    :class="{'ma_btn': count == GetConfig.pageNo}" @click="goToPage(count)">
                                <span v-text="count"></span>
                            </button>
                            <button @click="navPagination(2)" class="btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only"
                                    :disabled="GetConfig.pageNo == GetConfig.pageCount">
                                <i class="la la-angle-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <!--end::Section-->
            </div>
            <!--end::Form-->
        </div>
    </div>
    <script>
        new Vue({
            el: '#Vue_component_main',
            data: {
                Api_path: '{{env('API_PATH')}}',
                users: [],
                GetConfig: {
                    pageNo: 1,
                    limit: 15,
                    keyword: '',
                    filter: '',
                    filter_type: '',
                    pageCount: 0,
                }
            },
            methods: {
                GetUsers: function () {
                    let _this = this;
                    _this.GetConfig.pageCount = 0;
                    $.ajax({
                        url: _this.Api_path + "/user",
                        type: "get",
                        data: _this.GetConfig,
                        success: function (response) {
                            _this.users = response.data;
                            _this.GetConfig.pageCount = Math.ceil(response.totalData / _this.GetConfig.limit);
                        }
                    });
                },
                goToPage: function (pageNo) {
                    this.GetConfig.pageNo = pageNo;
                    this.GetUsers();
                },
                Search: function (pageNo) {
                    this.GetConfig.pageNo = 1;
                    this.GetUsers();
                },
                filterData: function (data) {
                    this.GetConfig.filter = data;
                    if (this.GetConfig.filter_type.length === 0) {
                        this.GetConfig.filter_type = 'DESC';
                    } else if (this.GetConfig.filter_type === 'DESC') {
                        this.GetConfig.filter_type = 'ASC';
                    } else if (this.GetConfig.filter_type === 'ASC') {
                        this.GetConfig.filter_type = 'DESC';
                    }
                    this.GetUsers();
                },
                DeleteItem: function (user_id) {
                    let _this = this;
                    swal({
                        title: "Are you sure?",
                        text: "Once Removed, you will not be able to recover this user!",
                        icon: "warning",
                        buttons: ['Cancel', 'Remove'],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: _this.Api_path + "/user",
                                type: "delete",
                                data: {id: user_id},
                                success: function (response) {
                                    if (response.status === 2000) {
                                        _this.GetUsers();
                                        swal("User has been Removed successfully!", {
                                            icon: "success",
                                        });
                                    }
                                }
                            });
                        } else {
                            swal("User is safe!", {
                                icon: "info",
                            });
                        }
                    });
                },
                navPagination: function(type){
                    if(type === 1){
                        if(this.GetConfig.pageNo !== 1){
                            this.GetConfig.pageNo--;
                        }
                    }else{
                        if(this.GetConfig.pageNo !== this.GetConfig.pageCount){
                            this.GetConfig.pageNo++;
                        }
                    }
                    this.GetUsers();
                },
            },
            created() {
                this.GetUsers();
            }
        });
    </script>
@endsection