@extends('layouts.structure')

@section('resources')

@endsection

@section('content')
    <div class="m-content" id="Vue_component_main">
        <form @submit.prevent="CreateUser">
            <div class="row">
                <div class="col-md-6">
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
											<span class="m-portlet__head-icon m--hide">
												<i class="la la-gear"></i>
											</span>
                                    <h3 class="m-portlet__head-text">
                                        Personal Information
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!--begin::Form-->
                        <div class="m-form m-form--fit m-form--label-align-right">
                            <div class="m-portlet__body" :class="{'disable-section': HttpRequest === true}">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Name
                                    </label>
                                    <div class="col-10 has-danger">
                                        <input v-model="CreateForm.name" name="name" class="form-control m-input" type="text" placeholder="Name">
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Email
                                    </label>
                                    <div class="col-10 has-danger">
                                        <input v-model="CreateForm.email" name="email" class="form-control m-input" type="email" placeholder="Email">
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Phone
                                    </label>
                                    <div class="col-10 has-danger">
                                        <input v-model="CreateForm.phone" name="phone" class="form-control m-input" type="text" placeholder="Phone">
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Location
                                    </label>
                                    <div class="col-10 has-danger">
                                        <select v-model="CreateForm.location" name="location" class="form-control m-input">
                                            <option value="">Select location</option>
                                            <option v-for="location in locations" :value="location.id" v-text="location.name"></option>
                                        </select>
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Profile
                                    </label>
                                    <div class="col-10 has-danger">
                                        <select v-model="CreateForm.user_type" name="user_type" class="form-control m-input">
                                            <option value="3">Basic user</option>
                                            <option value="2">Advance user</option>
                                        </select>
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Client
                                    </label>
                                    <div class="col-10 has-danger">
                                        <select v-model="CreateForm.client_id" name="client_id" class="form-control m-input">
                                            <option value="">Select Client</option>
                                            <option v-for="client in clients" :value="client.id" v-text="client.name"></option>
                                        </select>
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Form-->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
											<span class="m-portlet__head-icon m--hide">
												<i class="la la-gear"></i>
											</span>
                                    <h3 class="m-portlet__head-text">
                                        Log in Information
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!--begin::Form-->
                        <div class="m-form m-form--fit m-form--label-align-right">
                            <div class="m-portlet__body" :class="{'disable-section': HttpRequest === true}">
                                <div class="form-group m-form__group">
                                    <label>
                                        Password
                                    </label>
                                    <div class="has-danger">
                                        <input v-model="CreateForm.password" name="password" type="password" class="form-control m-input m-input--square" placeholder="Password">
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>
                                        Confirm Password
                                    </label>
                                    <div class="has-danger">
                                        <input v-model="CreateForm.password_confirmation" type="password" class="form-control m-input m-input--square" placeholder="Password">
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Form-->
                    </div>

                    <div class="col-md-12 text-right">
                        <a href="{{route('admin.users')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air ma_btn gray">
                            <span>
                                <i class="fa fa-times-circle"></i>
                                <span>Cancel</span>
                            </span>
                        </a>
                        <button type="submit" class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air ma_btn"
                                :disabled="HttpRequest === true"
                                :class="{'m-loader m-loader--right m-loader--light': HttpRequest === true}">
                            <span>
                                <i class="fa fa-check-circle"></i>
                                <span>Create</span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>
        new Vue({
            el: '#Vue_component_main',
            data: {
                Api_path: '{{env('API_PATH')}}',
                CreateForm: {
                    name: '',
                    email: '',
                    phone: '',
                    location: '',
                    user_type: 3,
                    client_id: '',
                    password: '',
                    password_confirmation: '',
                },
                HttpRequest: false,
                clients: [],
                profiles: [],
                locations: [],
            },
            methods: {
                GetClients: function () {
                    let _this = this;
                    $.ajax({
                        url: _this.Api_path+"/clients",
                        type: "get",
                        success: function (response) {
                            if (response.status === 2000) {
                                _this.clients = response.data;
                            }
                        }
                    });
                },
                GetProfiles: function () {
                    let _this = this;
                    $.ajax({
                        url: _this.Api_path+"/profiles",
                        type: "get",
                        success: function (response) {
                            if (response.status === 2000) {
                                _this.profiles = response.data;
                            }
                        }
                    });
                },
                GetLocations: function () {
                    let _this = this;
                    $.ajax({
                        url: _this.Api_path+"/locations",
                        type: "get",
                        success: function (response) {
                            if (response.status === 2000) {
                                _this.locations = response.data;
                            }
                        }
                    });
                },
                CreateUser: function () {
                    let _this = this;
                    _this.HttpRequest = true;
                    $('.form-control-feedback').html('');
                    $.ajax({
                        url: _this.Api_path+"/user",
                        type: "post",
                        data: _this.CreateForm,
                        success: function (response) {
                            _this.HttpRequest = false;
                            if (response.status === 2000) {
                                window.location.href = '{{route('admin.users')}}';
                            }else{
                                _this.ErrorHandaler(response.error)
                            }
                        }
                    });
                },
                ErrorHandaler: function (errors) {
                    $.each(errors, function (i, v) {
                        $('[name='+i+']').closest('.has-danger').find('.form-control-feedback').html(v);
                    })
                }
            },
            created(){
                this.GetClients();
                this.GetProfiles();
                this.GetLocations();
            }
        });
    </script>
@endsection