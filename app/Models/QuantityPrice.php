<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuantityPrice extends Model {
    protected $table = 'product_quantity_price';
}
