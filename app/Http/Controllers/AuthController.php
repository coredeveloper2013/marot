<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Repository\AuthRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController
{
    /*============*/
    // Authentication
    /*============*/
    public function login(Request $request)
    {
        $adminRepo = new AuthRepository();
        $rv = $adminRepo->login($request);
        return response()->json($rv, 200);
    }
    public function ResetPassword(Request $request)
    {
        $adminRepo = new AuthRepository();
        $rv = $adminRepo->ResetPassword($request);
        return response()->json($rv, 200);
    }
    public function logout(Request $request)
    {
        $adminRepo = new AuthRepository();
        $rv = $adminRepo->logout($request);
        return response()->json($rv, 200);
    }
}
