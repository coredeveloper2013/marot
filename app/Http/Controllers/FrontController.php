<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class FrontController extends BaseController
{
    /*============*/
    // Get User Info
    /*============*/
    public function GetUserInfo()
    {
        $info = Auth::guard('admins')->user();
        if ($info !== null) {
            $info = $info->toArray();
        }
        return $info;
    }

    public function Login()
    {
        $rv = array(
            'page' => 'Marot Login',
        );
        return view('modules.Auth.index')->with($rv);
    }
    /*============*/
    // Dashboard
    /*============*/
    public function Dashboard()
    {
        $rv = array(
            'page' => 'Dashboard',
            'nav' => 'Dashboard',
            'Auth' => $this->GetUserInfo(),
        );
        return view('modules.Dashboard.index')->with($rv);
    }

    /*============*/
    // Profile
    /*============*/
    public function Profile()
    {
        $rv = array(
            'page' => 'Profile',
            'nav' => 'Profile',
            'Auth' => $this->GetUserInfo(),
        );
        return view('modules.Profile.index')->with($rv);
    }


    /*============*/
    // Users
    /*============*/
    public function Users()
    {
        $rv = array(
            'page' => 'Users',
            'nav' => 'Users',
            'Auth' => $this->GetUserInfo(),
        );
        return view('modules.Users.index')->with($rv);
    }

    public function UsersCreate()
    {
        $rv = array(
            'page' => 'Users Create',
            'nav' => 'Users',
            'Auth' => $this->GetUserInfo(),
        );
        return view('modules.Users.create')->with($rv);
    }

    public function UsersEdit($id)
    {
        $rv = array(
            'page' => 'Users Create',
            'profile_id' => $id,
            'nav' => 'Users',
            'Auth' => $this->GetUserInfo(),
        );
        return view('modules.Users.edit')->with($rv);
    }

    /*============*/
    // Products
    /*============*/
    public function Products()
    {
        $rv = array(
            'page' => 'Products',
            'nav' => 'Products',
            'Auth' => $this->GetUserInfo(),
        );
        return view('modules.Products.index')->with($rv);
    }

    public function ProductsCreate()
    {
        $rv = array(
            'page' => 'Products Create',
            'nav' => 'Products',
            'Auth' => $this->GetUserInfo(),
        );
        return view('modules.Products.create')->with($rv);
    }

    public function ProductsEdit($id)
    {
        $rv = array(
            'page' => 'Product Edit',
            'product_id' => $id,
            'nav' => 'Products',
            'Auth' => $this->GetUserInfo(),
        );
        return view('modules.Products.edit')->with($rv);
    }

    public function ProductsView($id)
    {
        $rv = array(
            'page' => 'Product View',
            'product_id' => $id,
            'nav' => 'Products',
            'Auth' => $this->GetUserInfo(),
        );
        return view('modules.Products.view')->with($rv);
    }

    /*============*/
    // Products
    /*============*/
    public function Orders()
    {
        $rv = array(
            'page' => 'Orders',
            'nav' => 'Orders',
            'Auth' => $this->GetUserInfo(),
        );
        return view('modules.Orders.index')->with($rv);
    }
    public function OrdersEdit($id)
    {
        $rv = array(
            'page' => 'Order Edit',
            'order_id' => $id,
            'nav' => 'Orders',
            'Auth' => $this->GetUserInfo(),
        );
        return view('modules.Orders.edit')->with($rv);
    }



}
