<?php

namespace App\Http\Controllers\Media\Repo;

use App\Models\Media;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;
use Illuminate\Support\Facades\Validator;

Class MediaRepository
{
    public function Media_add($request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'image' => 'required|file',
            'media_type' => 'required|integer',
            'module_type' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }
        try {
            $filePath = null;
            if($input['media_type'] == 1){
                $filePath = Storage::disk('public')->put('/media/image', $input['image']);
            }else{
                $filePath = Storage::disk('public')->put('/media/file', $input['image']);
            }

            $image_file = $request->file('image');
            $attrs = array(
                'filename' => $image_file->getClientOriginalName(),
                'extension' => $image_file->getClientOriginalExtension(),
                'size' => $image_file->getSize(),
                'mimeType' => $image_file->getMimeType()
            );
            $new_name = basename($filePath);

            $MediaModel = new Media();
            $MediaModel->file_path = $new_name;
            $MediaModel->module_type = $input['module_type'];
            $MediaModel->media_type = $input['media_type'];
            $MediaModel->is_active = isset($input['is_active'])? $input['is_active'] : 0;
            $MediaModel->attrs = serialize($attrs);
            $MediaModel->created_at = Carbon::now();
            $MediaModel->save();
            return ['status' => 2000, 'data' => $MediaModel->toArray(), 'msg' => 'Data has been added successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }
    public function ImagePreview($request)
    {
        $input = $request->input();
        $filename = isset($input['img']) && $input['img'] != '' ? env('STORAGE_URL').'image/'. $input['img'] : env('ASSETS_PATH').'/image/avatar.jpg';
        if (isset($input['img']) && $input['img'] != '') {
            $extArr = explode('.', $input['img']);
            $ext = $extArr[count($extArr) - 1];
        } else {
            $ext = 'jpeg';
        }
        header('Content-Type: image/' . $ext);
        list($width, $height) = getimagesize($filename);
        $newwidth = isset($input['w']) && $input['w'] > 0 ? $input['w'] : 200; //200;//$width * $percent;
        $newheight = isset($input['h'])? $input['h'] : $newwidth;
//        $newheight = $height / ($width / $newwidth);
        $thumb = imagecreatetruecolor($newwidth, $newheight);
        if ($ext == 'jpeg' || $ext == 'jpg' || $ext == 'JPEG' || $ext == 'JPG') {
            $source = imagecreatefromjpeg($filename);
        } else if ($ext == 'png' || $ext == 'PNG') {
            $source = imagecreatefrompng($filename);
        } else if ($ext == 'gif' || $ext == 'GIF') {
            $source = imagecreatefromgif($filename);
        }
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        imagejpeg($thumb);
    }
    public function ImagePreviewlocal($request)
    {
        $input = $request->input();
        $filename = isset($input['img']) && $input['img'] != '' ? env('ASSETS_PATH').'/image/'. $input['img'] : env('ASSETS_PATH').'/image/avatar.jpg';
        if (isset($input['img']) && $input['img'] != '') {
            $extArr = explode('.', $input['img']);
            $ext = $extArr[count($extArr) - 1];
        } else {
            $ext = 'jpeg';
        }
        header('Content-Type: image/' . $ext);
        list($width, $height) = getimagesize($filename);
        $newwidth = isset($input['w']) && $input['w'] > 0 ? $input['w'] : 200; //200;//$width * $percent;
        $newheight = isset($input['h'])? $input['h'] : $newwidth;
//        $newheight = $height / ($width / $newwidth);
        $thumb = imagecreatetruecolor($newwidth, $newheight);
        if ($ext == 'jpeg' || $ext == 'jpg' || $ext == 'JPEG' || $ext == 'JPG') {
            $source = imagecreatefromjpeg($filename);
        } else if ($ext == 'png' || $ext == 'PNG') {
            $source = imagecreatefrompng($filename);
        } else if ($ext == 'gif' || $ext == 'GIF') {
            $source = imagecreatefromgif($filename);
        }
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        imagejpeg($thumb);
    }
}
