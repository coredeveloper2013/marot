<?php

namespace App\Http\Controllers\Media;

use App\Http\Controllers\Media\Repo\MediaRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class MediaController extends BaseController
{
    public function Media_add(Request $request)
    {
        $adminRepo = new MediaRepository();
        $rv = $adminRepo->Media_add($request);
        return response()->json($rv, 200);
    }
    public function ImagePreview(Request $request)
    {
        $adminRepo = new MediaRepository();
        $rv = $adminRepo->ImagePreview($request);
        return response()->json($rv, 200);
    }
    public function ImagePreviewlocal(Request $request)
    {
        $adminRepo = new MediaRepository();
        $rv = $adminRepo->ImagePreviewlocal($request);
        return response()->json($rv, 200);
    }

}
