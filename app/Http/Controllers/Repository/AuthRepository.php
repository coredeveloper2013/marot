<?php

namespace App\Http\Controllers\Repository;

use App\Users;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AuthRepository extends BaseController
{
    //========================
    // Authentication
    //========================
    public function login($request)
    {
        try {
            $input = $request->input();
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required|string',
            ]);
            if ($validator->fails()) {
                return ['status' => 5000, 'error' => $validator->errors()];
            }
            $credentials = array(
                'email' => $input['email'],
                'password' => $input['password'],
                'is_active' => 1,
            );
            $remember = isset($input['remember']) ? $input['remember'] : false;
            if (Auth::guard('admins')->attempt($credentials, $remember)) {
                return ['status' => 2000, 'msg' => 'Login Successful'];
            } else {
                return ['status' => 5000, 'error' => 'Invalid Credentials'];
            }
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }
    public function ResetPassword($request)
    {
        try {
            $input = $request->input();
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|exists:users,email,is_active,1',
            ]);
            if ($validator->fails()) {
                return ['status' => 5000, 'error' => $validator->errors()];
            }
            $new_password = substr(number_format(time() * rand(),0,'',''),0,8);
            $user = Users::where('email', $input['email'])->where('is_active', 1)->get()->first();
            $user->password = bcrypt($new_password);
            $user->update();
            $user['new_password'] = $new_password;
            Mail::send('emails.reset_password', ['user' => $user], function ($message) use ($user) {
                $message->to($user['email'], $user['name'])->subject('Impremta Marot: Reset Password');
                $message->from('no-reply@marot.cat', 'Impremta Marot');
            });
            return ['status' => 2000, 'msg' => "successful"];

        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }
    public function logout()
    {
        try {
            Auth::guard('admins')->logout();
            return ['status' => 2000, 'msg' => 'Logout Successful'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }
}
