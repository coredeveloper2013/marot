<?php

namespace App\Http\Controllers\Repository;

use App\Models\Clients;
use App\Models\Locations;
use App\Models\Orders;
use App\Models\Products;
use App\Models\Profiles;
use App\Users;
use App\Models\QuantityPrice;
use http\Client\Curl\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use Carbon\Carbon;
use phpDocumentor\Reflection\Location;

class AdminRepository extends BaseController
{
    //========================
    // Clients
    //========================
    public function ClientsGetAll($request)
    {
        try {
            $Model = new Clients();
            $data = $Model
                ->where('is_active', 1)
                ->orderBy('id', 'asc')
                ->get()->toArray();
            return ['status' => 2000, 'data' => $data, 'msg' => 'Data found successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    //========================
    // Profiles
    //========================
    public function ProfilesGetAll($request)
    {
        try {
            $Model = new Profiles();
            $data = $Model
                ->where('is_active', 1)
                ->orderBy('id', 'asc')
                ->get()->toArray();
            return ['status' => 2000, 'data' => $data, 'msg' => 'Data found successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }

    //========================
    // Locations
    //========================
    public function LocationsGetAll($request)
    {
        try {
            $user = Auth::guard('admins')->user();
            $Model = new Locations();
            $data = $Model
                ->where('is_active', 1)
                ->where(function ($query) use($user) {
                    if ($user['user_type'] != 1){
                        $query->where('idClient', $user['client_id']);
                    }
                })
                ->orderBy('id', 'asc')
                ->get()->toArray();
            return ['status' => 2000, 'data' => $data, 'msg' => 'Data found successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }


    //========================
    // User
    //========================
    public function UserCreate($request)
    {
        $input = $request->input();
        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'location' => 'required',
            'user_type' => 'required',
            'client_id' => 'required',
            'password' => 'required|confirmed',
        ]);
        if ($validator->fails()) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }
        try {
            $check = Users::where('email', $input['email'])->where('is_active', 1)->get()->count();
            if($check > 0){
                return ['status' => 5000, 'error' => ["email" => "The email has already been taken."]];
            }
            $Model = new Users();
            $Model->name = $input['name'];
            $Model->email = $input['email'];
            $Model->phone = $input['phone'];
            $Model->location = $input['location'];
            $Model->user_type = $input['user_type'];
            $Model->client_id = $input['client_id'];
            $Model->password = bcrypt($input['password']);
            $Model->created_at = Carbon::now();
            $Model->save();
            $user = $Model->toArray();
            $user['new_password'] = $input['password'];
            Mail::send('emails.create_user', ['user' => $user], function ($message) use ($user) {
                $message->to($user['email'], $user['name'])->subject('Impremta Marot: New Account on Impremta Marot');
                $message->from('no-reply@marot.cat', 'Impremta Marot');
            });
            return ['status' => 2000, 'msg' => 'Data has been added successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    public function UserEdit($request)
    {
        $input = $request->input();
        $validator = Validator::make($input, [
            'id' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'location' => 'required',
            'user_type' => 'required',
            'client_id' => 'required',
            'password' => 'sometimes|confirmed',
        ]);
        if ($validator->fails()) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }
        try {
            $Model = new Users();
            $check = $Model
                ->where('id', '!=', $input['id'])
                ->where('email', $input['email'])
                ->where('is_active', 1)
                ->count();
            if ($check > 0) {
                return ['status' => 5000, 'error' => ["email" => "The email has already been taken."]];
            }
            $data = $Model->where('id', $input['id'])->where('is_active', 1)->get()->first();
            if ($data == null) {
                return ['status' => 5000, 'error' => 'Invalid Request'];
            } else {
                $data->name = $input['name'];
                $data->email = $input['email'];
                $data->phone = $input['phone'];
                $data->location = $input['location'];
                $data->user_type = $input['user_type'];
                $data->client_id = $input['client_id'];
                isset($input['password']) ? $data->password = bcrypt($input['password']) : '';
                $data->update();
            };
            $data = $data->toArray();
            return ['status' => 2000, 'data' => $data, 'msg' => 'Data has been updated successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    public function UserGetAll($request)
    {
        $input = $request->input();
        try {
            $rv = [];
            $pageNo = isset($input['pageNo']) && (int)$input['pageNo'] > 0 ? $input['pageNo'] : 1;
            $take = isset($input['limit']) && (int)$input['limit'] > 0 ? $input['limit'] : 20;
            $keyword = isset($input['keyword']) && $input['keyword'] != '' ? $input['keyword'] : '';
            $skip = ($pageNo - 1) * $take;
            $orderBy = 'users.id';
            $orderBy_type = 'ASC';
            if (isset($input['filter']) && $input['filter'] != '') {
                if ($input['filter'] == 'email') {
                    $orderBy = 'users.email';
                } else if ($input['filter'] == 'client') {
                    $orderBy = 'clients.name';
                }
            }
            if (isset($input['filter_type']) && $input['filter_type'] != '') {
                $orderBy_type = $input['filter_type'];
            }

            $Model = new Users();
            $data = $Model
                ->select('clients.name as client_name', 'users.id', 'users.email')
                ->leftJoin('clients', 'users.client_id', 'clients.id')
                ->where('users.is_active', 1)
                ->where('users.user_type', '!=', 1)
                ->where(function ($query) use ($input) {
                    $query->where([
                        ['users.email', 'like', '%'.$input['keyword'].'%'],
                        ['users.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['clients.name', 'like', '%'.$input['keyword'].'%'],
                        ['users.is_active', '=', 1],
                    ]);
                })
                ->take($take)
                ->skip($skip)
                ->orderBy($orderBy, $orderBy_type)
                ->get()->toArray();
            $count = $Model
                ->leftJoin('clients', 'users.client_id', 'clients.id')
                ->where('users.is_active', 1)
                ->where(function ($query) use ($input) {
                    $query->where([
                        ['users.email', 'like', '%'.$input['keyword'].'%'],
                        ['users.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['clients.name', 'like', '%'.$input['keyword'].'%'],
                        ['users.is_active', '=', 1],
                    ]);
                })
                ->count();
            return ['status' => 2000, 'data' => $data, 'totalData' => $count, 'msg' => 'Data found successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    public function UserGetSingle($request)
    {
        $input = $request->input();
        $validator = Validator::make($input, [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }
        try {
            $id = $request['id'];
            $Model = new Users();
            $data = $Model->where('id', $id)->where('is_active', 1)->get()->first();
            if ($data != null) {
                return ['status' => 2000, 'data' => $data, 'msg' => 'Data found successfully'];
            }
            return ['status' => 5000, 'error' => 'Data not found'];

        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    public function UserDelete($request)
    {
        $input = $request->input();
        $validator = Validator::make($input, [
            'id' => 'required|exists:users,id,is_active,1',
        ]);
        if ($validator->fails()) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }
        try {
            $Model = new Users();
            $Model->where('id', $input['id'])->where('is_active', 1)->update([
                'is_active' => 0,
                'updated_at' => Carbon::now(),
            ]);
            return ['status' => 2000, 'msg' => 'Data has been deleted successfully'];

        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    //========================
    // User Profile
    //========================
    public function UserGetInfo($request)
    {
        $user = Auth::guard('admins')->user();
        return ['status' => 2000, 'data' => $user, 'msg' => 'Data has been added successfully'];
    }

    public function UserUpdateInfo($request)
    {
        $input = $request->input();
        $validator = Validator::make($input, [
            'id' => 'required|exists:users,id,is_active,1',
            'location' => 'sometimes',
            'password' => 'sometimes|confirmed',
        ]);
        if ($validator->fails()) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }
        try {
            $Model = new Users();

            $userInfo = $Model->where('id', $input['id'])->where('is_active', 1)->get()->first();
            isset($input['avatar']) ? $userInfo->avatar = $input['avatar'] : '';
            isset($input['location']) ? $userInfo->location = $input['location'] : '';
            isset($input['password']) ? $userInfo->password = bcrypt($input['password']) : '';
            $userInfo->update();

            return ['status' => 2000, 'msg' => 'Data has been updated successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    //========================
    // Products
    //========================
    public function ProductCreate($request)
    {
        $input = $request->input();
        $validator = Validator::make($input, [
            'name' => 'required',
            'code_product' => 'required|unique:products,code_product',
            'description' => 'required',
            'information_order' => 'required',
            'minimum_orders' => 'required|numeric',
            'client_id' => 'required|exists:clients,id,is_active,1',
            'file_path' => 'required',
        ]);
        if ($validator->fails()) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }

        // Validate line items
        $validationFlag = false;
        if ( count($input['quantity_price']) > 0 ) {
            $quantityCollector = [];
            foreach ( $input['quantity_price'] as $quantityPriceData ) {
                array_push($quantityCollector, $quantityPriceData);
            }
            // Now check first value is smaller than the second value and so on
            for ( $i = 0; $i <= count($quantityCollector); $i++ ) {
                if ( isset($quantityCollector[$i+1]) && ( $quantityCollector[$i]['quantity'] <= 0 || $quantityCollector[$i]['price'] <= 0 ) ) {
                    $validationFlag = true;
                    break;
                }
                if ( isset($quantityCollector[$i+1]) && $quantityCollector[$i]['quantity'] >= $quantityCollector[$i+1]['quantity'] ) { 
                    $validationFlag = true;
                    break;
                }
            }
        }

        if ( $validationFlag == true ) {
            return ['status' => 5000, 'error' => 'Your quantities are not serially correct or you have put zero in quantity or in price field!'];
        }

        try {
            $Model = new Products();
            $Model->name = $input['name'];
            $Model->code_product = $input['code_product'];
            $Model->description = $input['description'];
            $Model->information_order = $input['information_order'];
            $Model->minimum_orders = $input['minimum_orders'];
            $Model->client_id = $input['client_id'];
            $Model->file_path = $input['file_path'];
            $Model->created_at = Carbon::now();
            $Model->tags = $input['tags'];
            $Model->save();
            $rv = $Model->toArray();

            // Save proudct quantities and prices to quantity price table with last inserted id of product
            $lastInsertedId = $Model->id;
            if ( count($input['quantity_price']) > 0 ) {
                foreach ( $input['quantity_price'] as $quantityPriceData ) {
                    $quantityPrice = new QuantityPrice;
                    $quantityPrice->product_id = $lastInsertedId;
                    $quantityPrice->quantity = $quantityPriceData['quantity'];
                    $quantityPrice->price = $quantityPriceData['price'];
                    $quantityPrice->save();
                }
                $quantityPrice = new QuantityPrice;
                $quantityPrice->product_id = $lastInsertedId;
                $quantityPrice->quantity = 900000;
                $quantityPrice->price = $input['slash_price'];
                $quantityPrice->save();
            }

            return ['status' => 2000, 'data' => $rv, 'msg' => 'Data has been added successfully'];

        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }

    public function ProductEdit($request)
    {
        $input = $request->input();
        $validator = Validator::make($input, [
            'id' => 'required|exists:products,id,is_active,1',
            'name' => 'required',
            'code_product' => 'required|unique:products,code_product,' . $input['id'],
            'description' => 'required',
            'information_order' => 'required',
            'minimum_orders' => 'required|numeric',
            'client_id' => 'required|exists:clients,id,is_active,1',
            'file_path' => 'required',
        ]);
        if ( $validator->fails() ) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }

        // Validate line items
        $validationFlag = false;
        if ( count($input['quantity_price']) > 0 ) {
            $quantityCollector = [];
            foreach ( $input['quantity_price'] as $quantityPriceData ) {
                array_push($quantityCollector, $quantityPriceData);
            }
            // Now check first value is smaller than the second value and so on
            for ( $i = 0; $i <= count($quantityCollector); $i++ ) {
                if ( isset($quantityCollector[$i+1]) && ( $quantityCollector[$i]['quantity'] <= 0 || $quantityCollector[$i]['price'] <= 0 ) ) {
                    $validationFlag = true;
                    break;
                }
                if ( isset($quantityCollector[$i+1]) && $quantityCollector[$i]['quantity'] >= $quantityCollector[$i+1]['quantity'] ) { 
                    $validationFlag = true;
                    break;
                }
            }
        }

        if ( $validationFlag == true ) {
            return ['status' => 5000, 'error' => 'Your quantities are not serially correct or you have put zero in quantity or in price field!'];
        }

        try {
            $Model = new Products();
            $data = $Model->where('id', $input['id'])->where('is_active', 1)->update([
                "name" => $input['name'],
                "code_product" => $input['code_product'],
                "description" => $input['description'],
                "information_order" => $input['information_order'],
                "minimum_orders" => $input['minimum_orders'],
                "client_id" => $input['client_id'],
                "file_path" => $input['file_path'],
                "tags" => $input['tags'],
            ]);

            if ( count($input['quantity_price']) > 0 ) {
                // First delete all quantity and price based on product id
                $res = QuantityPrice::where('product_id', $input['id'])->delete();
                // Now insert these new quantity and price
                if ( count($input['quantity_price']) > 0 ) {
                    foreach ( $input['quantity_price'] as $quantityPriceData ) {
                        $quantityPrice = new QuantityPrice;
                        $quantityPrice->product_id = $input['id'];
                        $quantityPrice->quantity = $quantityPriceData['quantity'];
                        $quantityPrice->price = $quantityPriceData['price'];
                        $quantityPrice->save();
                    }
                    $quantityPrice = new QuantityPrice;
                    $quantityPrice->product_id = $input['id'];
                    $quantityPrice->quantity = 900000;
                    $quantityPrice->price = $input['slash_price'];
                    $quantityPrice->save();
                }
            }
            return ['status' => 2000, 'data' => $data, 'msg' => 'Data has been updated successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    public function ProductGetAll($request)
    {
        $input = $request->input();
        try {
            $rv = [];
            $user = Auth::guard('admins')->user();
            $pageNo = isset($input['pageNo']) && (int)$input['pageNo'] > 0 ? $input['pageNo'] : 1;
            $take = isset($input['limit']) && (int)$input['limit'] > 0 ? $input['limit'] : 20;
            $keyword = isset($input['keyword']) && $input['keyword'] != '' ? $input['keyword'] : '';
            $skip = ($pageNo - 1) * $take;
            $orderBy = 'products.id';
            $orderBy_type = 'ASC';
            if (isset($input['filter']) && $input['filter'] != '') {
                if ($input['filter'] == 'code_product') {
                    $orderBy = 'products.code_product';
                } else if ($input['filter'] == 'name') {
                    $orderBy = 'products.name';
                } else if ($input['filter'] == 'clients') {
                    $orderBy = 'clients.name';
                }
            }
            if (isset($input['filter_type']) && $input['filter_type'] != '') {
                $orderBy_type = $input['filter_type'];
            }

            $Model = new Products();
            $data = $Model
                ->select('clients.name as client_name', 'products.id', 'products.name', 'products.code_product', 'products.tags')
                ->leftJoin('clients', 'products.client_id', 'clients.id')
                ->where('products.is_active', 1)
                ->where(function ($query) use ($input) {
                    $query->where([
                        ['products.name', 'like', '%'.$input['keyword'].'%'],
                        ['products.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['products.tags', 'like', '%'.$input['keyword'].'%'],
                        ['products.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['products.code_product', 'like', '%'.$input['keyword'].'%'],
                        ['products.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['clients.name', 'like', '%'.$input['keyword'].'%'],
                        ['products.is_active', '=', 1],
                    ]);
                })
                ->where(function ($query) use($user) {
                    if ($user['user_type'] != 1){
                        $query->where('products.client_id', $user['client_id']);
                    }
                })
                ->take($take)
                ->skip($skip)
                ->orderBy($orderBy, $orderBy_type)
                ->get()->toArray();
            $count = $Model
                ->leftJoin('clients', 'products.client_id', 'clients.id')
                ->where('products.is_active', 1)
                ->where(function ($query) use ($input) {
                    $query->where([
                        ['products.name', 'like', '%'.$input['keyword'].'%'],
                        ['products.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['products.tags', 'like', '%'.$input['keyword'].'%'],
                        ['products.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['products.code_product', 'like', '%'.$input['keyword'].'%'],
                        ['products.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['clients.name', 'like', '%'.$input['keyword'].'%'],
                        ['products.is_active', '=', 1],
                    ]);
                })
                ->count();

            return ['status' => 2000, 'data' => $data, 'totalData' => $count, 'msg' => 'Data found successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    public function ProductGetSingle($request)
    {
        $input = $request->input();
        $validator = Validator::make($input, [
            'id' => 'required|exists:products,id,is_active,1',
        ]);
        if ($validator->fails()) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }
        try {
            $id = $request['id'];
            $Model = new Products();
            $data = $Model->where('id', $id)->where('is_active', 1)->get()->first();
            $quantityPrices = new QuantityPrice();
            $quantityPricesData = $quantityPrices->where('product_id', $id)->get();
            return ['status' => 2000, "data" => $data, 'quantityPricesData' => $quantityPricesData, 'msg' => 'Data removed successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    public function ProductDelete($request)
    {
        $input = $request->input();
        $validator = Validator::make($input, [
            'id' => 'required|exists:products,id,is_active,1',
        ]);
        if ($validator->fails()) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }
        try {
            $Model = new Products();
            $Model->where('id', $input['id'])->where('is_active', 1)->update([
                'is_active' => 0,
                'updated_at' => Carbon::now(),
            ]);
            return ['status' => 2000, 'msg' => 'Data has been deleted successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    //========================
    // Orders
    //========================
    public function OrderCreate($request)
    {
        $input = $request->input();
        $validator = Validator::make($input, [
            'product_id' => 'required|exists:products,id,is_active,1',
            'location' => 'required|exists:locations,id,is_active,1',
            'units' => 'required|not_in:0',
        ]);
        if ($validator->fails()) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }
        try {
            $product = Products::where('id', $input['product_id'])->where('is_active', 1)->get()->first();
            $Administor = Users::where('user_type', 1)->where('is_active', 1)->get()->first();

            $user = Auth::guard('admins')->user();
            $user_id = $user->id;
            $Model = new Orders();
            $Model->user_id = $user_id;
            $Model->product_id = $input['product_id'];
            $Model->order_code = isset($input['order_code']) ? $input['order_code'] : '';
            $Model->location = $input['location'];
            $Model->comments = $input['comments'];
            $Model->units = $input['units'];
            $Model->unit_price = $input['unit_price'];
            $Model->created_at = Carbon::now();
            $Model->tags = $input['producttags'];
            $Model->save();
            $order = $Model;
            $order['formatted_date'] = date("d M, Y", strtotime($order['created_at']));
            $order['location_address'] = $input['location_address'];
            $mailInfo = null;
            $mailInfo['user'] = $user;
            $mailInfo['order'] = $order;
            $mailInfo['final_price'] = $input['units'] * $input['unit_price'];
            $mailInfo['product'] = $product;

            Mail::send('emails.new_order', ['mailInfo' => $mailInfo], function ($message) use ($user) {
                $message->to($user['email'], $user['name'])->subject('Impremta Marot: New Order');
                $message->from('no-reply@marot.cat', 'Impremta Marot');
            });
            $mailInfo['user'] = $Administor;
            Mail::send('emails.new_order', ['mailInfo' => $mailInfo], function ($message) use ($Administor) {
                $message->to($Administor['email'], $Administor['name'])->subject('Marot: New Order');
                $message->from('no-reply@marot.cat', 'Impremta Marot');
            });

            return ['status' => 2000, 'msg' => 'Data has been added successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    public function OrderEdit($request)
    {
        $input = $request->input();
        $validator = Validator::make($input, [
            'id' => 'required|exists:orders,id,is_active,1',
        ]);
        if ($validator->fails()) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }
        try {
            $Model = new Orders();
            $data = $Model->where('id', $input['id'])->where('is_active', 1)->get()->first();
            if ($data !== null) {
                if (isset($input['order_code'])) {
                    if ($input['order_code'] != $data['order_code']) {
                        $data->order_code = $input['order_code'];
                        $data->visibility = isset($input['visibility']) ? $input['visibility'] : '';
                        $data->status = 2;
                        $data->update();
                    } else {
                        $data->visibility = isset($input['visibility']) ? $input['visibility'] : '';
                        $data->status = isset($input['status']) ? $input['status'] : $data['order_code'];
                        $data->update();
                    }
                } else {
                    if ($data['order_code'] == null) {
                        $data->visibility = isset($input['visibility']) ? $input['visibility'] : '';
                        $data->order_code = isset($input['order_code']) ? $input['order_code'] : '';
                        $data->status = isset($input['status']) ? $input['status'] : $data['status'];
                        $data->update();
                    } else {
                        $data->visibility = isset($input['visibility']) ? $input['visibility'] : '';
                        $data->order_code = isset($input['order_code']) ? $input['order_code'] : '';
                        $data->status = 2;
                        $data->update();
                    }
                }
            }
            return ['status' => 2000, 'data' => $data, 'msg' => 'Data has been updated successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    public function OrderGetAll($request)
    {
        $input = $request->input();
        try {
            $rv = [];
            $user = Auth::guard('admins')->user();
            $pageNo = isset($input['pageNo']) && (int)$input['pageNo'] > 0 ? $input['pageNo'] : 1;
            $take = isset($input['limit']) && (int)$input['limit'] > 0 ? $input['limit'] : 20;
            $keyword = isset($input['keyword']) && $input['keyword'] != '' ? $input['keyword'] : '';
            $skip = ($pageNo - 1) * $take;
            $orderBy = 'orders.id';
            $orderBy_type = 'DESC';
            if (isset($input['filter']) && $input['filter'] != '') {
                if ($input['filter'] == 'id') {
                    $orderBy = 'orders.id';
                } else if ($input['filter'] == 'name') {
                    $orderBy = 'products.name';
                } else if ($input['filter'] == 'code_product') {
                    $orderBy = 'products.code_product';
                } else if ($input['filter'] == 'units') {
                    $orderBy = 'orders.units';
                } else if ($input['filter'] == 'user_name') {
                    $orderBy = 'users.name';
                } else if ($input['filter'] == 'client') {
                    $orderBy = 'clients.name';
                } else if ($input['filter'] == 'order_code') {
                    $orderBy = 'orders.order_code';
                } else if ($input['filter'] == 'date') {
                    $orderBy = 'orders.created_at';
                }
            }
            if (isset($input['filter_type']) && $input['filter_type'] != '') {
                $orderBy_type = $input['filter_type'];
            }

            $Model = new Orders();
            $data = $Model
                ->select('products.name as product_name', 'products.code_product', 'products.tags', 'orders.*', 'users.name as user_name', 'users.client_id as company_id', 'clients.name as client_name')
                ->leftJoin('products', 'orders.product_id', 'products.id')
                ->leftJoin('users', 'orders.user_id', 'users.id')
                ->leftJoin('clients', 'users.client_id', 'clients.id')
                ->where('orders.is_active', 1)
                ->where(function ($query) use ($input) {
                    $query->where([
                        ['products.name', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['products.code_product', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['products.tags', 'like', '%'.$input['keyword'].'%'],
                        ['products.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['orders.order_code', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['orders.units', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['users.name', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['orders.created_at', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['clients.name', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                })
                ->where(function ($query) use($user) {
                    if ($user['user_type'] == 3){
                        $query->where('user_id', $user['id']);
                        $query->where('orders.visibility', 1);
                    }
                    if ($user['user_type'] == 2){
                        $query->where('users.client_id', $user['client_id']);
                        $query->where('orders.visibility', 1);
                    }
                })
                ->take($take)
                ->skip($skip)
                ->orderBy($orderBy, $orderBy_type)
                ->get()->toArray();
                $count = $Model
                ->select('products.name as product_name', 'products.code_product', 'products.tags', 'orders.*', 'users.name as user_name', 'clients.name as client_name')
                ->leftJoin('products', 'orders.product_id', 'products.id')
                ->leftJoin('users', 'orders.user_id', 'users.id')
                ->leftJoin('clients', 'users.client_id', 'clients.id')
                ->where('orders.is_active', 1)
                ->where(function ($query) use ($input) {
                    $query->where([
                        ['products.name', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['products.code_product', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['products.tags', 'like', '%'.$input['keyword'].'%'],
                        ['products.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['orders.order_code', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['orders.units', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['users.name', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['orders.created_at', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                    $query->orWhere([
                        ['clients.name', 'like', '%'.$input['keyword'].'%'],
                        ['orders.is_active', '=', 1],
                    ]);
                })
                ->where(function ($query) use($user) {
                    if ($user['user_type'] == 3){
                        $query->where('user_id', $user['id']);
                        $query->where('orders.visibility', 1);
                    }
                    if ($user['user_type'] == 2){
                        $query->where('users.client_id', $user['client_id']);
                        $query->where('orders.visibility', 1);
                    }
                })
                ->count();
            if (count($data) > 0) {
                foreach ($data as $item) {
                    $date = date("d/m/y", strtotime($item['created_at']));
                    $item['formatted_date'] = $date;
                    $rv[] = $item;
                }
            }
            return ['status' => 2000, 'data' => $rv, 'totalData' => $count, 'msg' => 'Data found successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }

    public function OrderGetSingle($request)
    {
        $input = $request->input();
        $validator = Validator::make($input, [
            'id' => 'required|exists:orders,id,is_active,1',
        ]);
        if ($validator->fails()) {
            return ['status' => 5000, 'error' => $validator->errors()];
        }
        try {
            $Model = new Orders();
            $data = $Model
                ->select('products.name as product_name', 'products.code_product', 'orders.*', 'users.name as user_name', 'users.email as user_email',
                    'users.phone as user_phone', 'clients.name as client_name', 'locations.name as address')
                ->leftJoin('products', 'orders.product_id', 'products.id')
                ->leftJoin('users', 'orders.user_id', 'users.id')
                ->leftJoin('clients', 'users.client_id', 'clients.id')
                ->leftJoin('locations', 'orders.location', 'locations.id')
                ->where('orders.id', $input['id'])
                ->where('orders.is_active', 1)
                ->get()->first();
            if ($data != null) {
                $data['formatted_date'] = date("d M, Y", strtotime($data['created_at']));
            }
            return ['status' => 2000, "data" => $data, 'msg' => 'Data removed successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }
    }
}
