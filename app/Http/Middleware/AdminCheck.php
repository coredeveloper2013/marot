<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminCheck
{
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('admins')->check()) {
            return redirect()->route('admin.login');
        }
        return $next($request);
    }
}
